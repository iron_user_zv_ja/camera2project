package com.ironbit.camera2sample.model.preferences;

import android.content.SharedPreferences;

import com.ironbit.camera2sample.Application;

public class SettingsCamera {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static final String PREFER_NAME = "settings_sample";
    public static final String KEY_CAMERAID = "cameraId";

    public SettingsCamera() {
        setSharedPreferences();
    }

    private void setSharedPreferences() {
        preferences = Application.getInstance().getSharedPreferences(PREFER_NAME, Application.getInstance().MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void cleanPreferences() {
        editor.clear();
        editor.commit();
    }

    public void setCameraId(String cameraId) {
        editor.putString(KEY_CAMERAID, cameraId);
        editor.commit();
    }

    public String getCameraId() {
        return preferences.getString(KEY_CAMERAID, "0");
    }

}
