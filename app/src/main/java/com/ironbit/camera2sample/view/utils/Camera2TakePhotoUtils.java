package com.ironbit.camera2sample.view.utils;

import android.annotation.TargetApi;
import android.content.Intent;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.ironbit.camera2sample.Application;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;


@TargetApi(Build.VERSION_CODES.KITKAT)
public class Camera2TakePhotoUtils implements Runnable {

    private ImageReader mImageReader;
    private final String NAME_DIRECTORY = "Camera";
    public static String pathPicture = "";

    public Camera2TakePhotoUtils(ImageReader mImageReader) {
        this.mImageReader = mImageReader;
    }

    @Override
    public void run() {
        Log.i("level", "ImageSaver--->run");
        Image image = mImageReader.acquireLatestImage();
        checkParentDir();
        File file;
        checkJpegDir();
        file = createJpeg();
        try {
            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
            try {
                save(bytes, file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            image.close();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    private void checkParentDir() {
        //create folder
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), NAME_DIRECTORY);
        if (!dir.exists()) {
            dir.mkdir();
        }
    }

    private void checkJpegDir() {
        //create subfolder
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), NAME_DIRECTORY);
        if (!dir.exists()) {
            dir.mkdir();
        }
    }

    private File createJpeg() {

        String dateCurrent = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());


        long time = System.currentTimeMillis();
        int random = new Random().nextInt(1000);
        //save on folder
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), NAME_DIRECTORY);
        Log.i("JpegSaver", "Urband" + "_" + dateCurrent + ".jpeg");
        return new File(dir, "Urband" + dateCurrent + ".jpeg");
    }

    private void save(byte[] bytes, File file) throws IOException {
        Log.i("JpegSaver", "save");
        OutputStream os = null;
        try {
            os = new FileOutputStream(file);
            os.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (os != null) {
                os.close();
            }
            galleryAddPic(file);
        }
    }

    private void galleryAddPic(File pathFile) {
        pathPicture = String.valueOf(pathFile);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(new File(pathPicture)));
        Application.getInstance().sendBroadcast(mediaScanIntent);
    }

    public static Uri getPathPicture() {
        return Uri.fromFile(new File(pathPicture));
    }
}
