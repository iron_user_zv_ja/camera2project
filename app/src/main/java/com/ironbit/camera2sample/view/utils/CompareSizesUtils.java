package com.ironbit.camera2sample.view.utils;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Size;

import java.util.Comparator;

public class CompareSizesUtils implements Comparator<Size> {

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public int compare(Size lhs, Size rhs) {
        // We cast here to ensure the multiplications won't overflow
        //CompareSizesByArea
        return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                (long) rhs.getWidth() * rhs.getHeight());
    }

}
