package com.ironbit.camera2sample;

import android.os.Handler;

/**
 * Created by Alejandro on 22/02/2016.
 */
public class Application extends android.app.Application {

    private static Application instance;
    private Handler handler = new Handler();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        handler = new Handler();
    }

    public static Application getInstance() {
        return instance;
    }

    public Handler getHandler() {
        return handler;
    }
}