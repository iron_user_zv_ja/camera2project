package com.ironbit.camera2sample.presenter.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;
import android.media.MediaActionSound;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ironbit.camera2sample.R;
import com.ironbit.camera2sample.model.preferences.SettingsCamera;
import com.ironbit.camera2sample.view.ui.AutoFitTextureView;
import com.ironbit.camera2sample.view.utils.Camera2TakePhotoUtils;
import com.ironbit.camera2sample.view.utils.CompareSizesUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class SelfieCamera2 extends Fragment implements View.OnClickListener, FragmentCompat.OnRequestPermissionsResultCallback {

    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final String FRAGMENT_DIALOG = "dialog";


    private static final int STATE_PREVIEW = 0;
    private static final int STATE_WAITING_CAPTURE = 1;
    private static final int STATE_TRY_CAPTURE_AGAIN = 2;
    private static final int STATE_TRY_DO_CAPTURE = 3;
    private int mState = STATE_PREVIEW;
    private Handler mPreviewHandler;
    private HandlerThread mHandlerThread;
    private AutoFitTextureView mPreviewView;
    private ImageReader mImageReader;
    private Size mPreviewSize;
    private String mCameraId;
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);
    private CameraDevice mCameraDevice;
    private CaptureRequest.Builder mPreviewBuilder;
    private CameraCaptureSession mSession;
    private ImageButton mBtnCapture;
    private MediaActionSound mMediaActionSound;
    private Surface mSurface;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    private SettingsCamera settingsCamera;
    private FrameLayout frame_shootView;
    private FrameLayout frame_buttonsView;
    private boolean takePhotoStatus = false;

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    public static SelfieCamera2 newInstance() {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   1  ");
        SelfieCamera2 fragment = new SelfieCamera2();
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   2  ");
        initShutter();
        settingsCamera = new SettingsCamera();
        return inflater.inflate(R.layout.home_selfie_camera2_fragment, null);
    }

    private void initShutter() {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   3  ");
        mMediaActionSound = new MediaActionSound();
        mMediaActionSound.load(MediaActionSound.SHUTTER_CLICK);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   4  ");
        frame_shootView = (FrameLayout) view.findViewById(R.id.selfie_camera_frame_shoot);
        frame_buttonsView = (FrameLayout) view.findViewById(R.id.selfie_camera_frame_buttons);
        super.onViewCreated(view, savedInstanceState);
        mPreviewView = (AutoFitTextureView) view.findViewById(R.id.selfie_texture_camera);
//        mPreviewView.setAlpha(0.0f);
        mBtnCapture = (ImageButton) view.findViewById(R.id.selfie_camera_btn_capture);
        mBtnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    takePicture();
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        /*
        mBtnCapture.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        try {
                            takePicture();
                            //Toast.makeText(getActivity(), "take picture", Toast.LENGTH_SHORT).show();
                        } catch (CameraAccessException e) {
                            e.printStackTrace();
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        try {
                            updateCameraPreviewSession();
                        } catch (CameraAccessException e) {
                            e.printStackTrace();
                        }
                        break;
                }
                return true;
            }
        });
        */

        ((ImageButton) view.findViewById(R.id.selfie_camera_btn_flip)).setOnClickListener(this);
        ((ImageButton) view.findViewById(R.id.selfie_camera_btn_cancel)).setOnClickListener(this);
        ((ImageButton) view.findViewById(R.id.selfie_camera_btn_share)).setOnClickListener(this);
    }

    private void takePicture() throws CameraAccessException {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   5  ");
        mState = STATE_WAITING_CAPTURE;
        mSession.setRepeatingRequest(initDngBuilder().build(), mSessionDngCaptureCallback, mPreviewHandler);

        frame_shootView.setVisibility(View.GONE);
        frame_buttonsView.setVisibility(View.VISIBLE);
        takePhotoStatus = true;
    }

    private CaptureRequest.Builder initDngBuilder() {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   6  ");
        CaptureRequest.Builder captureBuilder = null;
        try {
            captureBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);

            captureBuilder.addTarget(mImageReader.getSurface());
            // Required for RAW capture
            captureBuilder.set(CaptureRequest.STATISTICS_LENS_SHADING_MAP_MODE, CaptureRequest.STATISTICS_LENS_SHADING_MAP_MODE_ON);
            captureBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_OFF);
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            captureBuilder.set(CaptureRequest.SENSOR_EXPOSURE_TIME, (long) ((214735991 - 13231) / 2));
            captureBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, 0);
            captureBuilder.set(CaptureRequest.SENSOR_SENSITIVITY, (10000 - 100) / 2);
            //设置每秒30帧
            CameraManager cameraManager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
            CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(mCameraId);
            Range<Integer> fps[] = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
            captureBuilder.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, fps[fps.length - 1]);
        } catch (CameraAccessException e) {
            Log.i("initDngBuilder", "initDngBuilder");
            e.printStackTrace();
        }
        return captureBuilder;
    }

    private void initLooper() {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   7 ");
        mHandlerThread = new HandlerThread("Camera_2");
        mHandlerThread.start();
        mPreviewHandler = new Handler(mHandlerThread.getLooper());
    }

    @Override
    public void onResume() {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   8  ");
        super.onResume();

        if (takePhotoStatus) {
            frame_shootView.setVisibility(View.VISIBLE);
            frame_buttonsView.setVisibility(View.GONE);
        }

        initLooper();
        if (mPreviewView.isAvailable()) {
            openCamera(mPreviewView.getWidth(), mPreviewView.getHeight());
            Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   8.1  ");
        } else {
            mPreviewView.setSurfaceTextureListener(mSurfaceTextureListener);
            Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   8.2  ");
        }

    }


    private void openCamera(int width, int height) {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   9  ");
        if (Build.VERSION.SDK_INT >= 23 && getActivity().checkSelfPermission(Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
            return;
        }

        setUpCameraOutputs(width, height);
        configureTransform(width, height);
        Activity activity = getActivity();
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            manager.openCamera(mCameraId, DeviceStateCallback, mPreviewHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        }
    }

    private void setUpCameraOutputs(int width, int height) {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   10  ");
        Activity activity = getActivity();
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {

            String cameraId = settingsCamera.getCameraId();
            Log.i("debug:", "camera-out: " + settingsCamera.getCameraId());
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);

            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            Size largest;

            //    largest = Collections.max(Arrays.asList(map.getOutputSizes(ImageFormat.RAW_SENSOR)), new CompareSizesByArea());
            //    initImageReader(largest, ImageFormat.RAW_SENSOR);
            //    mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class), width, height, largest);

            largest = Collections.max(Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)), new CompareSizesUtils());
            initImageReader(largest, ImageFormat.JPEG);
            mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class), width, height, largest);

            int orientation = getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                mPreviewView.setAspectRatio(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            } else {
                mPreviewView.setAspectRatio(mPreviewSize.getHeight(), mPreviewSize.getWidth());
            }
            mCameraId = cameraId;
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
    }

    private void initImageReader(Size size, int format) {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   11  ");
        mImageReader = ImageReader.newInstance(size.getWidth(), size.getHeight(), format, /*maxImages*/7);
        mImageReader.setOnImageAvailableListener(mOnImageAvailableListener, mPreviewHandler);

    }


    private ImageReader.OnImageAvailableListener mOnImageAvailableListener = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(ImageReader reader) {
            Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   12 ");
            new Thread(new Camera2TakePhotoUtils(reader)).start();
            mMediaActionSound.play(MediaActionSound.SHUTTER_CLICK);
        }
    };

    private Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio) {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   13 ");
        List<Size> bigEnough = new ArrayList<Size>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getHeight() == option.getWidth() * h / w &&
                    option.getWidth() >= width && option.getHeight() >= height) {
                bigEnough.add(option);
            }
        }

        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesUtils());
        } else {
            return choices[0];
        }
    }

    private void configureTransform(int viewWidth, int viewHeight) {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   14 ");
        Activity activity = getActivity();
        if (null == mPreviewView || null == mPreviewSize || null == activity) {
            return;
        }
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        mPreviewView.setTransform(matrix);
    }


    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   14.1 ");
            openCamera(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };

    private CameraDevice.StateCallback DeviceStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   15 ");
            mCameraOpenCloseLock.release();
            mCameraDevice = camera;
            try {
                createCameraPreviewSession();
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   16 ");
            mCameraOpenCloseLock.release();
            camera.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   17 ");
            mCameraOpenCloseLock.release();
            camera.close();
            mCameraDevice = null;
            Toast.makeText(getActivity(), "onError,error--->" + error, Toast.LENGTH_SHORT).show();
        }
    };

    private void initSurface() {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   18 ");
        SurfaceTexture texture = mPreviewView.getSurfaceTexture();
        assert texture != null;
        texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
        mSurface = new Surface(texture);
    }

    private void createCameraPreviewSession() throws CameraAccessException {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   19 ");
        initSurface();
        mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

        mPreviewBuilder.addTarget(mSurface);
        mState = STATE_PREVIEW;
        mCameraDevice.createCaptureSession(Arrays.asList(mSurface, mImageReader.getSurface()), mSessionPreviewStateCallback, mPreviewHandler);
    }

    private CameraCaptureSession.StateCallback mSessionPreviewStateCallback = new CameraCaptureSession.StateCallback() {

        @Override
        public void onConfigured(CameraCaptureSession session) {
            Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   20 ");
            if (null == mCameraDevice) {
                return;
            }
            mSession = session;
            try {
                mPreviewBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                mPreviewBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
                session.setRepeatingRequest(mPreviewBuilder.build(), mSessionCaptureCallback, mPreviewHandler);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onConfigureFailed(CameraCaptureSession session) {
            Activity activity = getActivity();
            if (null != activity) {
                Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void updateCameraPreviewSession() throws CameraAccessException {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   21 ");
        mPreviewBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
        mPreviewBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
        mState = STATE_PREVIEW;
        mSession.setRepeatingRequest(mPreviewBuilder.build(), mSessionCaptureCallback, mPreviewHandler);
    }

    private TotalCaptureResult mDngResult;
    private CameraCaptureSession.CaptureCallback mSessionDngCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);
            Log.i("mSessionDng", "CaptureCallback" + "mSessionDngCaptureCallback");
            mDngResult = result;
//            takeDngPicture(result);
//            mMediaActionSound.play(MediaActionSound.SHUTTER_CLICK);
        }
    };
    private CameraCaptureSession.CaptureCallback mSessionCaptureCallback = new CameraCaptureSession.CaptureCallback() {

        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
            mSession = session;

            //if (!PreferenceHelper.getCameraFormat(getActivity()).equals("DNG")) {
            //    Log.i("debug:", "take getcameraFORMAAAAAAT   + 5.1");
            checkState(result);
            //}


        }

        @Override
        public void onCaptureProgressed(CameraCaptureSession session, CaptureRequest request, CaptureResult partialResult) {
            Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   22 ");
            mSession = session;
            /*
            if (!PreferenceHelper.getCameraFormat(getActivity()).equals("DNG")) {
                Log.i("debug:", "take getcameraFORMAAAAAAT   + 6.1");
                checkState(partialResult);
            }
            */
            checkState(partialResult);
        }

        private void checkState(CaptureResult result) {
//            mFrameBitmap = mPreviewView.getBitmap();
//            mMainHandler.sendEmptyMessage(1);
            switch (mState) {
                case STATE_PREVIEW:
                    // NOTHING
                    break;
                case STATE_WAITING_CAPTURE:
                    int afState = result.get(CaptureResult.CONTROL_AF_STATE);
                    Log.i("checkState", "afState--->" + afState);
                    if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState || CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState
                            || CaptureResult.CONTROL_AF_STATE_PASSIVE_FOCUSED == afState || CaptureResult.CONTROL_AF_STATE_PASSIVE_UNFOCUSED == afState) {
                        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                        Log.i("checkState", "llegando capa uno,aeState--->" + aeState);
                        if (aeState == null || aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            Log.i("checkState", "llegando capa 2");
                            mState = STATE_TRY_DO_CAPTURE;
                            doStillCapture();
                        } else {
                            mState = STATE_TRY_CAPTURE_AGAIN;
                            tryCaptureAgain();
                        }
                    }
                    break;
                case STATE_TRY_CAPTURE_AGAIN:
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null ||
                            aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                            aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        mState = STATE_TRY_DO_CAPTURE;
                    }
                    break;
                case STATE_TRY_DO_CAPTURE:
                    aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        mState = STATE_TRY_DO_CAPTURE;
                        doStillCapture();
                    }
                    break;
            }
        }

    };

    private void doStillCapture() {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   24 ");
        try {
            CaptureRequest.Builder captureBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(mImageReader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            captureBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
            int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));
            mSession.stopRepeating();
            mSession.setRepeatingRequest(captureBuilder.build(), new CameraCaptureSession.CaptureCallback() {
            }, mPreviewHandler);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    private void tryCaptureAgain() {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   25 ");
        mPreviewBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
        try {
            mSession.capture(mPreviewBuilder.build(), mSessionCaptureCallback, mPreviewHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.selfie_camera_btn_flip:
                String id = settingsCamera.getCameraId();
                Log.i("debug:", "flip-ent: " + id);
                if (id.equals("0")) {
                    settingsCamera.setCameraId("1");

                } else {
                    settingsCamera.setCameraId("0");
                    //mPreviewSize = new Size(1280, 720);
                    /*
                    mCameraOpenCloseLock.release();
                    mCameraDevice.close();
                    mCameraDevice = null;
                    initLooper();
                    if (mPreviewView.isAvailable()) {
                        openCamera(mPreviewView.getWidth(), mPreviewView.getHeight());
                        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   8.1  ");
                    } else {
                        mPreviewView.setSurfaceTextureListener(mSurfaceTextureListener);
                        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   8.2  ");
                    }
                    */
                }


                Fragment currentFragment = getActivity().getFragmentManager().findFragmentById(R.id.selfie_frame_container);
                if (currentFragment instanceof SelfieCamera2) {
                    FragmentTransaction fragTransaction = (getActivity()).getFragmentManager().beginTransaction();
                    fragTransaction.detach(currentFragment);
                    fragTransaction.attach(currentFragment);
                    fragTransaction.commit();
                }



                /*
                if (mSession != null) {
                    mSession.close();
                    mSession = null;
                }
                if (mCameraDevice != null) {
                    mCameraDevice.close();
                    mCameraDevice = null;
                }

                initLooper();
                if (mPreviewView.isAvailable()) {
                    openCamera(mPreviewView.getWidth(), mPreviewView.getHeight());
                    Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   8.1  ");
                } else {
                    mPreviewView.setSurfaceTextureListener(mSurfaceTextureListener);
                    Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   8.2  ");
                }
                */


                Log.i("debug:", "flip-out: " + settingsCamera.getCameraId());
                break;
            case R.id.selfie_camera_btn_cancel:

                mCameraOpenCloseLock.release();
                mCameraDevice.close();
                mCameraDevice = null;

                initLooper();
                if (mPreviewView.isAvailable()) {
                    openCamera(mPreviewView.getWidth(), mPreviewView.getHeight());
                    Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   8.1  ");
                } else {
                    mPreviewView.setSurfaceTextureListener(mSurfaceTextureListener);
                    Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   8.2  ");
                }

                if (takePhotoStatus) {
                    takePhotoStatus = false;
                }

                frame_shootView.setVisibility(View.VISIBLE);
                frame_buttonsView.setVisibility(View.GONE);


                break;
            case R.id.selfie_camera_btn_share:
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);

                Log.i("debug: path: ", String.valueOf(Camera2TakePhotoUtils.getPathPicture()));
                shareIntent.putExtra(Intent.EXTRA_STREAM, Camera2TakePhotoUtils.getPathPicture());
                shareIntent.setType("image/jpeg");
                startActivity(Intent.createChooser(shareIntent, "Urband - Compartir"));
                break;
        }
    }

    @Override
    public void onPause() {
        Log.i("debug:", "camera starting ¨¨¨¨¨¨¨¨¨¨¨¨¨¨   26 ");
        super.onPause();
        closeCamera();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }












    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mSession) {
                mSession.close();
                mSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mImageReader) {
                mImageReader.close();
                mImageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }


    private void requestCameraPermission() {
        if (FragmentCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            new ConfirmationDialog().show(getChildFragmentManager(), FRAGMENT_DIALOG);
        } else {
            FragmentCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    public static class ConfirmationDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity())
                    .setMessage("Se necesitan permisos para la cámara")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FragmentCompat.requestPermissions(parent,
                                    new String[]{Manifest.permission.CAMERA},
                                    REQUEST_CAMERA_PERMISSION);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Activity activity = parent.getActivity();
                                    if (activity != null) {
                                        activity.finish();
                                    }
                                }
                            })
                    .create();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                ErrorDialog.newInstance("Se necesitan permisos para la cámara")
                        .show(getChildFragmentManager(), FRAGMENT_DIALOG);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public static class ErrorDialog extends DialogFragment {

        private static final String ARG_MESSAGE = "message";

        public static ErrorDialog newInstance(String message) {
            ErrorDialog dialog = new ErrorDialog();
            Bundle args = new Bundle();
            args.putString(ARG_MESSAGE, message);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Activity activity = getActivity();
            return new AlertDialog.Builder(activity)
                    .setMessage(getArguments().getString(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            activity.finish();
                        }
                    })
                    .create();
        }

    }

}
