package com.ironbit.camera2sample.presenter.activities;

import android.app.Activity;
import android.os.Bundle;

import com.ironbit.camera2sample.R;
import com.ironbit.camera2sample.presenter.fragments.SelfieCamera2;


public class HomeSelfieCamera2 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_selfie_camera2);
        getFragmentManager().beginTransaction().replace(R.id.selfie_frame_container, SelfieCamera2.newInstance()).commit();
    }


}
